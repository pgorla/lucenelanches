var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var babelify = require('babelify');

var dependencies = [
	'react',
  	'react-dom'
];
var scriptsCount = 0;

// ----------------------------------------------------------------------------
gulp.task('scripts', function () {
    bundleApp(false);
});

gulp.task('deploy', function (){
	bundleApp(true);
});

gulp.task('default', ['scripts']);

function bundleApp(isProduction) {
	var appBundler = browserify({
    	entries: 'src/main/resources/templates/app/app.js',
    	debug: true
  	})

  	appBundler
	  	.transform("babelify", {presets: ["es2015", "react"]})
	    .bundle()
	    .on('error',gutil.log)
	    .pipe(source('bundle.js'))
	    .pipe(gulp.dest('src/main/resources/templates/web'));
}
