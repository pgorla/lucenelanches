package br.com.lucenelanches.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.com.lucenelanches.endpoints", "br.com.lucenelanches.service", "br.com.lucenelanches.repository"})
@EnableJpaRepositories(basePackages = {"br.com.lucenelanches.repository"})
@EntityScan(basePackages = {"br.com.lucenelanches.entity"})
public class AppConfig {

    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);
    }

}
