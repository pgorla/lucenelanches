package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Sauce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "sauceRepository")
@Component
public interface SauceRepository extends JpaRepository<Sauce, Long> {

}
