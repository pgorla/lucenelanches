package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Bread;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "breadRepository")
@Component
public interface BreadRepository extends JpaRepository<Bread, Long> {

}
