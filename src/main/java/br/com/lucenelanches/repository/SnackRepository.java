package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Snack;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "snackRepository")
@Component
public interface SnackRepository extends CrudRepository<Snack, Long> {

}
