package br.com.lucenelanches.repository;


import br.com.lucenelanches.entity.Spice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository(value = "spiceRepository")
@Component
public interface SpiceRepository extends JpaRepository<Spice, Long> {

}
