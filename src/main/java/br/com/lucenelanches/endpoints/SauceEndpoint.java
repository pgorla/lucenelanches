package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.entity.Sauce;
import br.com.lucenelanches.service.SauceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SauceEndpoint {

    @Autowired
    private SauceService sauceService;

    @RequestMapping("/sauce")
    List<Sauce> home() {

        return sauceService.getAll();
    }


}
