package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.entity.Filling;
import br.com.lucenelanches.service.FillingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FillingEndpoint {

    @Autowired
    private FillingService fillingService;

    @RequestMapping("/filling")
    List<Filling> home() {

        return fillingService.getAll();
    }


}
