package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.entity.Snack;
import br.com.lucenelanches.service.SnackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SnackEndpoint {

    @Autowired
    private SnackService snackService;

    @RequestMapping("/snack")
    Iterable<Snack> home() {

        return snackService.getAll();
    }


}
