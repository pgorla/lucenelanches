package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.entity.Spice;
import br.com.lucenelanches.service.SpiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SpiceEndpoint {

    @Autowired
    private SpiceService spiceService;

    @RequestMapping("/spice")
    List<Spice> home() {
        return spiceService.getAll();
    }


}
