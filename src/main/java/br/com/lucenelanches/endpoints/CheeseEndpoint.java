package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.entity.Cheese;
import br.com.lucenelanches.service.CheeseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CheeseEndpoint {

    @Autowired
    private CheeseService cheeseService;

    @RequestMapping("/cheese")
    List<Cheese> home() {
        List<Cheese> cheeses = new ArrayList<>();
        Cheese cheese1 = new Cheese();
        cheese1.setId(1L);
        cheese1.setCheese("parto");

        Cheese cheese2 = new Cheese();
        cheese2.setId(2L);
        cheese2.setCheese("chedar");

        cheeses.add(cheese1);
        cheeses.add(cheese2);

        return cheeses;
    }


}
