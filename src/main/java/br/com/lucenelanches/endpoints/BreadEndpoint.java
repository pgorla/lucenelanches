package br.com.lucenelanches.endpoints;

import br.com.lucenelanches.entity.Bread;
import br.com.lucenelanches.service.BreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BreadEndpoint {

    @Autowired
    private BreadService breadService;

    @RequestMapping("/bread")
    List<Bread> home() {

        return breadService.getAll();
    }


}
