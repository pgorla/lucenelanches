package br.com.lucenelanches.entity;

import javax.persistence.*;

@Entity(name = "snacks")
public class Snack {

    @Id
    @Column(name = "idt_snack")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bread", referencedColumnName = "idt_bread")
    Bread bread;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cheese", referencedColumnName = "idt_cheese")
    Cheese cheese;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "filling", referencedColumnName = "idt_filling")
    Filling filling;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sauce", referencedColumnName = "idt_sauce")
    Sauce sauce;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "spice", referencedColumnName = "idt_spice")
    Spice spice;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "salad", referencedColumnName = "idt_salad")
    Salad salad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bread getBread() {
        return bread;
    }

    public void setBread(Bread bread) {
        this.bread = bread;
    }

    public Cheese getCheese() {
        return cheese;
    }

    public void setCheese(Cheese cheese) {
        this.cheese = cheese;
    }

    public Filling getFilling() {
        return filling;
    }

    public void setFilling(Filling filling) {
        this.filling = filling;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public Spice getSpice() {
        return spice;
    }

    public void setSpice(Spice spice) {
        this.spice = spice;
    }

    public Salad getSalad() {
        return salad;
    }

    public void setSalad(Salad salad) {
        this.salad = salad;
    }
}
