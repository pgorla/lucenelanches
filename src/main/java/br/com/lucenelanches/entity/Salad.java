package br.com.lucenelanches.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "salad")
public class Salad {

    @Id
    @Column(name = "idt_salad")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "salad")
    String salad;

    @Column(name = "value")
    BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
