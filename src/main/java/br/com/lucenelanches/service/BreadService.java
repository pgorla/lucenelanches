package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Bread;
import br.com.lucenelanches.entity.Cheese;
import br.com.lucenelanches.repository.BreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "breadService")
@Component
public class BreadService {

    @Autowired
    BreadRepository breadRepository;

    public List<Bread> getAll() {
        return breadRepository.findAll();
    }
}
