package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Filling;
import br.com.lucenelanches.repository.FillingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "fillingService")
@Component
public class FillingService {

    @Autowired
    FillingRepository fillingRepository;

    public List<Filling> getAll(){
        return fillingRepository.findAll();
    }
}
