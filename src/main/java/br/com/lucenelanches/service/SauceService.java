package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Sauce;
import br.com.lucenelanches.repository.SauceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "sauceService")
@Component
public class SauceService {

    @Autowired
    SauceRepository sauceRepository;

    public List<Sauce> getAll(){
        return sauceRepository.findAll();
    }
}
