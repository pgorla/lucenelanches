package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Snack;
import br.com.lucenelanches.repository.SnackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "snackService")
@Component
public class SnackService {

    @Autowired
    SnackRepository snackRepository;

    public Iterable<Snack> getAll(){
        return snackRepository.findAll();
    }
}
