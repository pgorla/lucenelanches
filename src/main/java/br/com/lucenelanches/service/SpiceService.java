package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Spice;
import br.com.lucenelanches.repository.SpiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "spiceService")
@Component
public class SpiceService {

    @Autowired
    SpiceRepository spiceRepository;

    public List<Spice> getAll(){
        return spiceRepository.findAll();
    }
}
