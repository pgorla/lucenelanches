package br.com.lucenelanches.service;

import br.com.lucenelanches.entity.Salad;
import br.com.lucenelanches.repository.SaladRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "saladService")
@Component
public class SaladService {

    @Autowired
    SaladRepository saladRepository;

    public List<Salad> getAll(){
        return saladRepository.findAll();
    }
}
